#!/usr/bin/perl -w
use strict;


###This script is used to extract the sequence info for each exon ############
##awk function is needed in this script#################
####### Input/output files ########################
my $Annotation_GFF3 = "Mus_musculus.GRCm38.87.chr.gff3";
my $Annotation_GFF3_Exon = "Mus_musculus.GRCm38.87.exon.gff3";
my $Reference_Genome = "Mus_musculus.GRCm38.dna.primary_assembly.fa";
open(OUT_1, ">Mus_musculus.GRCm38.87.exon.info");
open(OUT_2, ">Mus_musculus.GRCm38.87.exon.seq");
#####################################################

######Step 1: store the seq for each chr into hash ################
my $Chr_ID = "";
my $Chr_Seq = "";
my %Chr_Seq = ();
open(IN_1, "$Reference_Genome");
my @Ref_Seq = <IN_1>; chomp @Ref_Seq;
close IN_1;
for(my $i=0; $i<@Ref_Seq; $i++){
    if($Ref_Seq[$i] =~ /^>/){ ##Header line
	if($i==0){ ##First line of the file
	    $Chr_ID = substr($Ref_Seq[$i],1,index($Ref_Seq[$i]," ")-1);
	}
	else{
	    $Chr_Seq{$Chr_ID} = $Chr_Seq;
	    $Chr_ID = substr($Ref_Seq[$i],1,index($Ref_Seq[$i]," ")-1);
	    $Chr_Seq = "";
	}
    }
    else{
	$Chr_Seq = "$Chr_Seq$Ref_Seq[$i]";
	if($i==scalar(@Ref_Seq)-1){ ##last line of the file
	    $Chr_Seq{$Chr_ID} = $Chr_Seq;
	    $Chr_Seq = "";
	}
    }
}
@Ref_Seq = (); #Release the space
##########################################################################

######Step 2: extract exon info from Annotation GFF3 data ################
my $Cmd = "awk \'{if(\$3==\"exon\"){print \$0}}\' $Annotation_GFF3 >$Annotation_GFF3_Exon";
print "$Cmd\n";
system($Cmd);
open(IN_2, "$Annotation_GFF3_Exon");
my @Exon_Info = <IN_2>; chomp @Exon_Info;
#########################################################################

#####Step 3: Get the seq for each exon ##################################
my %Exon_ID_Uniq = (); #only analyze uniq exon ids
foreach my $Exon_Info_Line (@Exon_Info){
    my @Exon_Info_Line_Array = split(/\t/,$Exon_Info_Line);
    my @Exon_Meta_Info_Array = split(/\;/,$Exon_Info_Line_Array[8]);
    my $Exon_Chr = $Exon_Info_Line_Array[0];
    my $Exon_Strand = $Exon_Info_Line_Array[6];
    my $Exon_Pos1 = $Exon_Info_Line_Array[3];
    my $Exon_Pos2 = $Exon_Info_Line_Array[4];
    my $Transcript_ID = "";
    my $Exon_ID = "";
    my $Exon_Rank = "";
    my $Exon_Seq = "";
    foreach my $Exon_Meta_Info_Array_Element (@Exon_Meta_Info_Array){
	if($Exon_Meta_Info_Array_Element =~ /^Parent=transcript:/){
	    $Transcript_ID = substr($Exon_Meta_Info_Array_Element, index($Exon_Meta_Info_Array_Element,":")+1);
	}
	elsif($Exon_Meta_Info_Array_Element =~ /^exon_id=/){
	    $Exon_ID = substr($Exon_Meta_Info_Array_Element, index($Exon_Meta_Info_Array_Element,"=")+1);
	}
	elsif($Exon_Meta_Info_Array_Element =~ /^rank=/){
	    $Exon_Rank = substr($Exon_Meta_Info_Array_Element, index($Exon_Meta_Info_Array_Element,"=")+1);
	}
    }
    print OUT_1 "$Exon_ID\t$Exon_Chr\t$Exon_Strand\t$Exon_Pos1\t$Exon_Pos2\t$Transcript_ID\t$Exon_Rank\n";

    if(!defined($Exon_ID_Uniq{$Exon_ID})){
	if($Exon_Strand eq "+"){
	    $Exon_Seq = substr($Chr_Seq{$Exon_Chr},$Exon_Pos1-1,$Exon_Pos2-$Exon_Pos1+1);
	}
	elsif($Exon_Strand eq "-"){
	    $Exon_Seq = reverse (substr($Chr_Seq{$Exon_Chr},$Exon_Pos1-1,$Exon_Pos2-$Exon_Pos1+1));
	    $Exon_Seq  =~ tr/ACGTacgt/TGCAtgca/;
	}
	print OUT_2 "$Exon_ID\_$Exon_Chr\_$Exon_Strand\_$Exon_Pos1\_$Exon_Pos2\t$Exon_Seq\n";
	$Exon_ID_Uniq{$Exon_ID} = "T";
    }
}
unlink "$Annotation_GFF3_Exon";
close OUT_1;
close OUT_2;
#################################################################################
