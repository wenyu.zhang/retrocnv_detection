## This document shows the progress to generate exon-exon junction library for a target species (with mus musculus as an example).

Required program:
* awk


### Step 1. Prepare the original data (Ensembl version87)
1) gene annotation data: ftp://ftp.ensembl.org/pub/release-87/gff3/mus_musculus/Mus_musculus.GRCm38.87.chr.gff3.gz
2) reference genome data: ftp://ftp.ensembl.org/pub/release-87/fasta/mus_musculus/dna/Mus_musculus.GRCm38.dna.primary_assembly.fa.gz

### Step 2: Extract the exon information from the gff3 annotation data and the sequence for each exon
>./1.Get_Exon_Seq.pl

### Step 3: Generate exon-exon junction datasets for consecutive exons (with filtering on exon and intron sizes)
>./2.Generate_Exon_Junction.pl

Note: One may need to apply further filtering on the exon-exon junctions, e.g., only keeping exon-exon junctions for protein-coding genes.
