#!/usr/bin/perl -w
use strict;

##This script is used to genereate exon-exon junction dataset################

##Parameters for exon-exon junction generating ###################
my $Exon_Capture = 100; #Take 100bp from each exon of the junction. (In case exon length<100, then take the full length, but minimum exon should be >=50bp)
my $Exon_Size_Cutoff = 50; #Minimum size of exon size.
my $Intron_Size_Cutoff = 50; #Minimum size of intron size between two consecutive exons
##################################################################

##Input and outfiles #############################################
open(IN_1, "Mus_musculus.GRCm38.87.exon.seq");
open(IN_2, "Mus_musculus.GRCm38.87.exon.info");
open(OUT, ">Mus_musculus.GRCm38.87.exon_junction.fa");
my @Exon_Seq = <IN_1>; chomp @Exon_Seq;
my @Exon_Info = <IN_2>; chomp @Exon_Info;
close IN_1;
close IN_2;
##################################################################

##Step 1: store exon seq into hash######################
my %Exon_Seq = ();
foreach my $Exon_Seq_Line (@Exon_Seq){
    my @Exon_Seq_Line_Array = split(/\t/,$Exon_Seq_Line);
    my @Exon_Meta_Info = split(/\_/,$Exon_Seq_Line_Array[0]);
    my $Exon_ID = $Exon_Meta_Info[0];
    my $Exon_Seq = $Exon_Seq_Line_Array[1];
    $Exon_Seq{$Exon_ID} = $Exon_Seq;
}
###################################################################

##Step 2: Process for each pair of consecutive exons ###############
my %Uniq_Exon_Junction = (); #only analyze for unique exon junctions
for(my $i=0; $i<scalar(@Exon_Info)-1; $i++){
    my @Exon_Info_1_Array = split(/\t/,$Exon_Info[$i]);
    my @Exon_Info_2_Array = split(/\t/,$Exon_Info[$i+1]);
    my $Exon_ID_1 = $Exon_Info_1_Array[0];
    my $Exon_ID_2 = $Exon_Info_2_Array[0];
    my $Exon_1_Pos1 = $Exon_Info_1_Array[3];
    my $Exon_1_Pos2 = $Exon_Info_1_Array[4];
    my $Exon_2_Pos1 = $Exon_Info_2_Array[3];
    my $Exon_2_Pos2 = $Exon_Info_2_Array[4];
    my $Exon_Transcript_1 = $Exon_Info_1_Array[5];
    my $Exon_Transcript_2 = $Exon_Info_2_Array[5];
    my $Exon_Rank_1 = $Exon_Info_1_Array[6];
    my $Exon_Rank_2 = $Exon_Info_2_Array[6];

    if(($Exon_Transcript_1 eq $Exon_Transcript_2) && abs($Exon_Rank_2-$Exon_Rank_1)==1){ ##Consecutive exons
	my $Order_Exon_1; #The exon in the first order actually
	my $Order_Exon_2; #The exon in the second order actually
	my $Intron_Len = 0;
	my $Junction_ID = "";
	my $Junction_Out_Str = "";
	if($Exon_Rank_2>$Exon_Rank_1){ #the rank of exon is increasing...
	    $Order_Exon_1 = $Exon_ID_1;
	    $Order_Exon_2 = $Exon_ID_2;
	}
	else{
	    $Order_Exon_1 = $Exon_ID_2;
            $Order_Exon_2 = $Exon_ID_1;
	}
	
	$Intron_Len = $Exon_2_Pos1-$Exon_1_Pos2-1;
	$Junction_ID="$Order_Exon_1\_$Order_Exon_2";
	if(!defined($Uniq_Exon_Junction{$Junction_ID})){
	    if(length($Exon_Seq{$Order_Exon_1})>=$Exon_Size_Cutoff && length($Exon_Seq{$Order_Exon_2})>=$Exon_Size_Cutoff && $Intron_Len>=$Intron_Size_Cutoff){
		$Junction_Out_Str = Get_Exon_Junction_Seq($Order_Exon_1,$Order_Exon_2,length($Exon_Seq{$Order_Exon_1}),length($Exon_Seq{$Order_Exon_2}));
		print OUT "$Junction_Out_Str\n";
	    }
	    $Uniq_Exon_Junction{$Junction_ID} = "T";
	}
    }
}
close OUT;
######################################################################################################



################Sub functons used in the script ######################################################
sub Get_Exon_Junction_Seq{
    (my $Exon_1,my $Exon_2,my $Exon_1_Len,my $Exon_2_Len)=@_;
    my $Out_Exon_1_Len = 0; ## How many bases to output for exon_1
    my $Out_Exon_2_Len = 0; ## How many bases to output for exon_2
    my $Out_Exon_1_Seq = 0;
    my $Out_Exon_2_Seq = 0;
    my $Out_Header= ""; ## Header for output
    my $Out_Seq = ""; ## Seq for junction
    my $Out_Str = ""; ## combine the info from above two

    if($Exon_1_Len>=$Exon_Capture){
        $Out_Exon_1_Len = $Exon_Capture;
        $Out_Exon_1_Seq = substr($Exon_Seq{$Exon_1},$Exon_1_Len-$Exon_Capture); ## Last 100bp of exon_1
    }
    else{
        $Out_Exon_1_Len = $Exon_1_Len;
        $Out_Exon_1_Seq = $Exon_Seq{$Exon_1}; ##Otherwise full length of the exon
    }

    if($Exon_2_Len>=100){
        $Out_Exon_2_Len = $Exon_Capture;
        $Out_Exon_2_Seq = substr($Exon_Seq{$Exon_2},0,$Exon_Capture); ## First 100bp of exon_2
    }
    else{
        $Out_Exon_2_Len = $Exon_2_Len;
        $Out_Exon_2_Seq = $Exon_Seq{$Exon_2}; ##Otherwise full length of the exon
    }

    $Out_Header = ">$Exon_1\_$Out_Exon_1_Len\_$Exon_2\_$Out_Exon_2_Len";
    $Out_Seq = "$Out_Exon_1_Seq$Out_Exon_2_Seq";
    $Out_Str = "$Out_Header\n$Out_Seq";

    return $Out_Str;
}
