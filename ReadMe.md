# RetroCNV hunter

This repository contains the documents to show an example of detection of retroCNV based on short read sequencing data.

* 1. Create_Exon_Junction_Lib:
The folder shows the steps to generate exon-exon junction library.

* 2. RetroCNV_Simulation:
This folder shows an example of simulating retroCNV insertion in the reference genome.

* 3. RetroCNV_Gene_Calling:
This folder shows the steps to call and visualize the retroposed parental gene/transcript.

* 4. RetroCNV_Insertion_Site_Inference
This folder shows the steps to infer the insertion site of retroCNV gene.



Question and bug report: wyzhang@evolbio.mpg.de
