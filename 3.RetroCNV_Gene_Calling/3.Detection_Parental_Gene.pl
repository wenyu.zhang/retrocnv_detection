#!/usr/bin/perl -w
use strict;
use List::MoreUtils qw(uniq);

require HQ_Alignment_Check;

####This script is used to get the evidence of exon-intron-exon junction(presence of parental gene) based on the alignment to the reference genome####################
##samtools program is also used in this script.

##############Controlling parameters##############################
my $MapQ_Cutoff = 20; ## Cutoff for mapping quality
my $Score_Diff = 5; ## Cutoff for the difference of best alignment score and second best score
my $Score_Alignment_Ratio = 0.8; #Cutoff of the ratio between alignment score and aligned length(requiring consideration on the reward/penalty of match/mismatch of alignment setting)
my $Span_Len = 30; ##Cutoff for the spanning read length on both sides of exons
my $Num_Evi = 2; ##Number of distinct spanning reads (distinct mapping positions) to support the presence of exon-intron and intron-exon
##################################################################

############Input and output files###############################
my $Exon_Info = "../1.Create_Exon_Junction_Lib/Mus_musculus.GRCm38.87.exon.info";
my $Detected_Exon_Junction = "Exon_Junction_Detected.list";
my $Exon_Junction_Bed = "Exon_Junction_Detected.bed"; ##The positions of exons detected from last step on the exon-exon junctions
my $Reference_Genome_Alignment = "Target_Seq_Extraction_100bp_30X_Reference_Genome_Aligned_Sorted_nodup.bam";
my $Final_Exon_Exon_Junction = "Exon_Junction_Final.list";
open(OUT_1, ">$Exon_Junction_Bed");
open(OUT_2, ">$Final_Exon_Exon_Junction");
################################################################

#########Step 1: Generate bed file for the previously detected exon-exon junctions#########################
my %Exon_Loc = ();
open(IN_1, "$Exon_Info");
my @Exon_Info = <IN_1>; chomp @Exon_Info;
close IN_1;
foreach my $Exon_Info_Line (@Exon_Info){
    my @Exon_Info_Line_Array = split(/\t/,$Exon_Info_Line);
    my $Exon_ID = $Exon_Info_Line_Array[0];
    my $Exon_Chr = $Exon_Info_Line_Array[1];
    #my $Exon_Strand = $Exon_Info_Line_Array[2];
    my $Exon_Start = $Exon_Info_Line_Array[3];
    my $Exon_End = $Exon_Info_Line_Array[4];
    $Exon_Loc{$Exon_ID} = "$Exon_Chr\t$Exon_Start\t$Exon_End";
}

open(IN_2, "$Detected_Exon_Junction");
my @Detcted_Exon_Juction = <IN_2>; chomp @Detcted_Exon_Juction;
close IN_2;
foreach my $Exon_Juction_ID (@Detcted_Exon_Juction){
    my @Exon_Juction_ID_Array = split(/\_/,$Exon_Juction_ID);
    my $Exon_ID_1 = $Exon_Juction_ID_Array[0];
    my $Exon_ID_2 = $Exon_Juction_ID_Array[2];
    my $Exon_1_Loc = $Exon_Loc{$Exon_ID_1};
    my $Exon_2_Loc = $Exon_Loc{$Exon_ID_2};
    print OUT_1 "$Exon_1_Loc\t$Exon_Juction_ID\n";
    print OUT_1 "$Exon_2_Loc\t$Exon_Juction_ID\n";
}
@Exon_Info = ();
@Detcted_Exon_Juction = ();
close OUT_1;
###########################################################################################################

#########Step 2: Get "good" HQ alignment result ##########################################################
my @Good_Alignment = ();

#1) Selecte the alignment data within the above bed region
my $Seletected_Alignment = "Selected_Alignment.sam";
my $Cmd = "samtools view -L $Exon_Junction_Bed $Reference_Genome_Alignment >$Seletected_Alignment";
print "$Cmd\n";
system($Cmd);

#2) Get the HQ alignment results
open(IN_3, "$Seletected_Alignment");
my @Selected_Alignment = <IN_3>; chomp @Selected_Alignment;
close IN_3;
foreach my $Alignment_Line (@Selected_Alignment){
    my $HQ_Flag = HQ_Alignment_Check::HQ_Alignment_Check($Alignment_Line,$MapQ_Cutoff,$Score_Diff,$Score_Alignment_Ratio);
    if($HQ_Flag eq "T"){
	@Good_Alignment = (@Good_Alignment,$Alignment_Line);
    }
}
@Selected_Alignment = ();
unlink "$Seletected_Alignment";
#############################################################################################################

#######Step 3: Assign exon-intron, intron-exon supporting reads#############################################
my @Support_Evi = ();
open(IN_4, "$Exon_Junction_Bed");
my @Exon_Junction_Loc = <IN_4>; chomp @Exon_Junction_Loc;
close IN_4;
for(my $i=0; $i<@Exon_Junction_Loc; $i=$i+2){
    my @Exon_1_Loc = split(/\t/,$Exon_Junction_Loc[$i]);
    my $Exon_Junction_ID = $Exon_1_Loc[3];
    my $Exon_Chr = $Exon_1_Loc[0];
    my $Exon_1_Start = $Exon_1_Loc[1];
    my $Exon_1_End = $Exon_1_Loc[2];
    my @Exon_2_Loc = split(/\t/,$Exon_Junction_Loc[$i+1]);
    my $Exon_2_Start = $Exon_2_Loc[1];
    my $Exon_2_End = $Exon_2_Loc[2];

    foreach my $Good_Alignment_Line (@Good_Alignment){
	my @Good_Alignment_Line_Array = split(/\t/,$Good_Alignment_Line);
	my $Alignment_Chr = $Good_Alignment_Line_Array[2];
	if("$Alignment_Chr" eq "$Exon_Chr"){
	    my $Alignment_Pos = $Good_Alignment_Line_Array[3];
	    my $Read_Len = length($Good_Alignment_Line_Array[9]);
	    my $CIAGR = $Good_Alignment_Line_Array[5];
	    my $Clipped_Len = HQ_Alignment_Check::Get_Soft_Clipping_Len($CIAGR);
	    my $Aligned_Len = $Read_Len - $Clipped_Len;
	    my $Exon_1_Span_Len=0; 
	    my $Intron_1_Span_Len=0;
	    my $Intron_2_Span_Len=0; 
	    my $Exon_2_Span_Len=0;
	    if($Exon_1_Start < $Exon_2_Start){ #Sense strand: increasing rank order
		$Exon_1_Span_Len = $Exon_1_End-$Alignment_Pos+1;
		$Intron_1_Span_Len = $Aligned_Len-$Exon_1_Span_Len;
		$Intron_2_Span_Len = $Exon_2_Start-$Alignment_Pos+1;
		$Exon_2_Span_Len = $Aligned_Len-$Intron_2_Span_Len;
	    }
	    else{ #Anti-Sense strand: decreasing rank order
		$Exon_2_Span_Len = $Exon_2_End-$Alignment_Pos+1;
                $Intron_2_Span_Len = $Aligned_Len-$Exon_2_Span_Len;
                $Intron_1_Span_Len = $Exon_1_Start-$Alignment_Pos+1;
                $Exon_1_Span_Len = $Aligned_Len-$Intron_1_Span_Len;
	    }
	    
	    if($Exon_1_Span_Len >= $Span_Len && $Intron_1_Span_Len >= $Span_Len){
		my $Support_Evi = "$Exon_Junction_ID\tExon_Intron\t$Good_Alignment_Line";
		@Support_Evi = (@Support_Evi,$Support_Evi);
	    }
	    elsif($Exon_2_Span_Len >= $Span_Len && $Intron_2_Span_Len >= $Span_Len){
		my $Support_Evi = "$Exon_Junction_ID\tIntron_Exon\t$Good_Alignment_Line";
		@Support_Evi = (@Support_Evi,$Support_Evi);
	    }
	}
    }    
}
@Good_Alignment = ();
unlink "$Exon_Junction_Bed";
################################################################################################

####Step 4: Output exon junctions with sufficient number of supporting reads###################
my %Exon_Intron = ();
my %Intron_Exon = ();

foreach my $Support_Evi_Line (@Support_Evi){
    my @Support_Evi_Line_Array = split(/\t/,$Support_Evi_Line);
    my $Exon_Junction = $Support_Evi_Line_Array[0];
    my $Alignment_Pos = $Support_Evi_Line_Array[5];
    if($Support_Evi_Line =~ /Intron\_Exon/){
	if(!(defined $Intron_Exon{$Exon_Junction})){
	    $Intron_Exon{$Exon_Junction} = $Alignment_Pos;
	}
	else{
	    $Intron_Exon{$Exon_Junction} = "$Intron_Exon{$Exon_Junction}\t$Alignment_Pos";
	}
    }
    elsif($Support_Evi_Line =~ /Exon\_Intron/){
	if(!(defined $Exon_Intron{$Exon_Junction})){
	    $Exon_Intron{$Exon_Junction} = $Alignment_Pos;
	}
	else{
	    $Exon_Intron{$Exon_Junction} = "$Exon_Intron{$Exon_Junction}\t$Alignment_Pos";
	}
    }
}

foreach my $Exon_Junction (keys %Exon_Intron){
    if(defined($Exon_Intron{$Exon_Junction}) && defined($Intron_Exon{$Exon_Junction})){
        my @Exon_Intron_Pos= split(/\t/, $Exon_Intron{$Exon_Junction});
        my @Intron_Exon_Pos = split(/\t/, $Intron_Exon{$Exon_Junction});
        my $Exon_Intron_Support = scalar(uniq @Exon_Intron_Pos);
        my $Intron_Exon_Support = scalar(uniq @Intron_Exon_Pos);
        if($Exon_Intron_Support >= $Num_Evi && $Intron_Exon_Support >= $Num_Evi){
            print OUT_2 "$Exon_Junction\n";
        }
    }
}
close OUT_2;
###################################################################################################
