#!/usr/bin/perl -w
use strict;
use List::MoreUtils qw(uniq);

require HQ_Alignment_Check;

####This script is used to get the evidence of exon-exon junction(intron loss) based on the alignment to the exon-exon junction dataset####################

##############Controlling parameters##############################
my $MapQ_Cutoff = 20; ## Cutoff for mapping quality
my $Score_Diff = 5; ## Cutoff for the difference of best alignment score and second best score
my $Score_Alignment_Ratio = 0.8; #Cutoff of the ratio between alignment score and aligned length(requiring consideration on the reward/penalty of match/mismatch of alignment setting)
my $Span_Len = 30; ##Cutoff for the spanning read length on both sides of exons
my $Num_Evi = 2; ##Number of distinct spanning reads (differnt mapping postions) to support the presence of exon-exon junction
##################################################################

############Input and output files###############################
my $Exon_Junction_Alignment = "Target_Seq_Extraction_100bp_30X_Exon_Junction_Aligned.sam";
my $Detected_Exon_Junction = "Exon_Junction_Detected.list";
open(OUT, ">$Detected_Exon_Junction");
################################################################

##########Setp 1: Extract "good" exon-exon alignments##################
open(IN, "$Exon_Junction_Alignment");
my @Exon_Junction_Alignment = <IN>; chomp @Exon_Junction_Alignment;
close IN;
my @Good_Alignment = ();

foreach my $Alignment_Line (@Exon_Junction_Alignment){
    my $HQ_Flag = HQ_Alignment_Check::HQ_Alignment_Check($Alignment_Line,$MapQ_Cutoff,$Score_Diff,$Score_Alignment_Ratio);
    if($HQ_Flag eq "T"){
	my @Alignment_Line_Array = split(/\t/,$Alignment_Line);
	my $Exon_Junction_ID = $Alignment_Line_Array[2];
        my @Exon_Junction_ID_Array = split(/\_/,$Exon_Junction_ID);
	my $Exon_1_Len = $Exon_Junction_ID_Array[1]; ## extraction length for exon 1
	my $Exon_2_Len = $Exon_Junction_ID_Array[3];
	my $Alignment_Pos = $Alignment_Line_Array[3];
	my $CIAGR = $Alignment_Line_Array[5];
	my $Read_Len = length($Alignment_Line_Array[9]);
	my $Clipped_Len = HQ_Alignment_Check::Get_Soft_Clipping_Len($CIAGR);
	my $Aligned_Len = $Read_Len - $Clipped_Len;
	my $Exon_1_Span_Len = $Exon_1_Len-$Alignment_Pos+1;
	my $Exon_2_Span_Len = $Aligned_Len-$Exon_1_Span_Len;
	if($Exon_1_Span_Len >= $Span_Len && $Exon_2_Span_Len >= $Span_Len){
	    @Good_Alignment = (@Good_Alignment,$Alignment_Line);
	}
    }
}
@Exon_Junction_Alignment = ();
#################################################################

###########Step 2: Get the number of distinct support reads (with different mapping positions) for each exon junction#################
my %Supp_Evi = ();

foreach my $Alignment_Line (@Good_Alignment){
    my @Alignment_Line_Array = split(/\t/,$Alignment_Line);
    my $Exon_Junction_ID = $Alignment_Line_Array[2];
    my $Alignment_Pos = $Alignment_Line_Array[3];
    if(!defined($Supp_Evi{$Exon_Junction_ID})){
	$Supp_Evi{$Exon_Junction_ID} = $Alignment_Pos;
    }
    else{
	$Supp_Evi{$Exon_Junction_ID} = "$Supp_Evi{$Exon_Junction_ID},$Alignment_Pos";
    }
}
@Good_Alignment = ();
#######################################################################################################################################

#######Step 3: Only output the exon-exon junctions with minimum number of required supporting evidences################################
foreach my $Exon_Junction (keys %Supp_Evi){
    my @Support_Pos = split(/\,/,$Supp_Evi{$Exon_Junction});
    my $Num_Support_Pos = scalar(uniq(@Support_Pos));
    if($Num_Support_Pos >= $Num_Evi){
	print OUT "$Exon_Junction\n";
    }
}
close OUT;
########################################################################################################################################
