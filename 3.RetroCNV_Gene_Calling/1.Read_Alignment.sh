#!/bin/bash

###This script is used to generate alignment results to both the exon-exon junction dataset and reference genome by using BWA mem

##Step 1: index on both dataset
bwa index ../1.Create_Exon_Junction_Lib/Mus_musculus.GRCm38.87.exon_junction.fa
bwa index ../1.Create_Exon_Junction_Lib/Mus_musculus.GRCm38.dna.primary_assembly.fa

##Step 2: Alignment of simulation reads to exon-exon junction dataset (single end mode)
bwa mem -t 2 -M ../1.Create_Exon_Junction_Lib/Mus_musculus.GRCm38.87.exon_junction.fa ../2.RetroCNV_Simulation/Target_Seq_Extraction_100bp_30X1.fq | samtools view -F 4 ->./Target_Seq_Extraction_100bp_30X1_Exon_Junction_Aligned.sam
bwa mem -t 2 -M ../1.Create_Exon_Junction_Lib/Mus_musculus.GRCm38.87.exon_junction.fa ../2.RetroCNV_Simulation/Target_Seq_Extraction_100bp_30X2.fq | samtools view -F 4 ->./Target_Seq_Extraction_100bp_30X2_Exon_Junction_Aligned.sam
cat Target_Seq_Extraction_100bp_30X1_Exon_Junction_Aligned.sam Target_Seq_Extraction_100bp_30X2_Exon_Junction_Aligned.sam >Target_Seq_Extraction_100bp_30X_Exon_Junction_Aligned.sam
rm Target_Seq_Extraction_100bp_30X1_Exon_Junction_Aligned.sam Target_Seq_Extraction_100bp_30X2_Exon_Junction_Aligned.sam

##Step 3: Alignment of simulation reads to the reference genome and remove PCR-duplicates

#1) Alignment to the reference genome (paired end mode)
bwa mem -t 2 -M ../1.Create_Exon_Junction_Lib/Mus_musculus.GRCm38.dna.primary_assembly.fa ../2.RetroCNV_Simulation/Target_Seq_Extraction_100bp_30X1.fq ../2.RetroCNV_Simulation/Target_Seq_Extraction_100bp_30X2.fq | samtools view -F 4 -bS ->Target_Seq_Extraction_100bp_30X_Reference_Genome_Aligned.bam

#2) sort the alignment data
samtools sort ./Target_Seq_Extraction_100bp_30X_Reference_Genome_Aligned.bam -o ./Target_Seq_Extraction_100bp_30X_Reference_Genome_Aligned_Sorted.bam
rm ./Target_Seq_Extraction_100bp_30X_Reference_Genome_Aligned.bam

#3) Mark and remove PCR duplicates with PICARD
java -jar $PICARD MarkDuplicates I=./Target_Seq_Extraction_100bp_30X_Reference_Genome_Aligned_Sorted.bam O=./Target_Seq_Extraction_100bp_30X_Reference_Genome_Aligned_Sorted_nodup.bam METRICS_FILE=./Target_Seq_Extraction_100bp_30X_Reference_Genome_Aligned.sorted.duplicate.metrics REMOVE_DUPLICATES=true ASSUME_SORTED=true MAX_RECORDS_IN_RAM=500000 VALIDATION_STRINGENCY=LENIENT
rm ./Target_Seq_Extraction_100bp_30X_Reference_Genome_Aligned_Sorted.bam

