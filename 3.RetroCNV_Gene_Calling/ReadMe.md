## This document shows the process to detect gene retroposition events and generate the bed format file for visualization

Required programs
* bwa
* samtools
* PICARD

### Step 1: Alignment of the simulated reads to exon-exon junction dataset (single-end mode) and reference genome (paired end mode)
> bash 1.Read_Alignment.sh

### Step 2: Detect the intron loss events (exon-exon junction)
> perl 2.Detection_Exon_Junction.pl

### Step 3: Detect the presence of exon-intron-exon junction (parental sequences)
> perl 3.Detection_Parental_Gene.pl

### Step 4: Generate the bed format data for the visualization in igv
1) Blue blocks show the refseq annotation
2) Red blocks show the detected retroposed exon-exon junctions
3) Grey blocks show the excluded exon-exon junctions (due to small exon or intron)

![Gene retroposition](Image/Detected_Retroposition.JPG)