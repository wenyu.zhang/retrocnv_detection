#!/usr/bin/perl -w
use strict;

#####This script is used to generate the bed file of exon junction for visualization######################

#####Parameters to define small exon/intron (not used for exon junction) ###################
my $Exon_Size_Cutoff = 50; #Minimum size of exon size.
my $Intron_Size_Cutoff = 50; #Minimum size of intron size between two consecutive exons
#########################################################################################

#####Input and output file ##############################################################
my $Retroposed_Transcript_List = "Retroposed_Transcript.list"; ##transcripts that are retroposed, detected from above pipeline
my $Exon_Info = "../1.Create_Exon_Junction_Lib/Mus_musculus.GRCm38.87.exon.info";
my $Detected_Exon_Junction = "Exon_Junction_Final.list"; ##Exon junctions with detected both intron loss and presence of exon-intron-exon
my $Retroposition_Event_Bed = "Detected_Retroposition.bed"; ## output on the blocks of exon-exon junctions, also small exon-exon junctions
open(OUT, ">$Retroposition_Event_Bed");
print OUT "track name=Retroposition description=\"Exon-exon junction\" useScore=1\n";
#########################################################################################

#####Step 1: Store the transcript exon list and exon position into hashs#################
my %Exon_List = ();
my %Exon_Loc = ();
open(IN_1, "$Exon_Info");
my @Exon_Info = <IN_1>; chomp @Exon_Info;
close IN_1;
foreach my $Exon_Info_Line (@Exon_Info){
    my @Exon_Info_Line_Array = split(/\t/,$Exon_Info_Line);
    my $Exon_ID = $Exon_Info_Line_Array[0];
    my $Exon_Chr = $Exon_Info_Line_Array[1];
    my $Exon_Strand = $Exon_Info_Line_Array[2];
    my $Exon_Pos1 = $Exon_Info_Line_Array[3];
    my $Exon_Pos2 = $Exon_Info_Line_Array[4];
    my $Transcript_ID = $Exon_Info_Line_Array[5];
    $Exon_Loc{$Exon_ID} = "$Exon_Chr\:$Exon_Strand\:$Exon_Pos1\:$Exon_Pos2";
    if(!defined($Exon_List{$Transcript_ID})){
	$Exon_List{$Transcript_ID} = $Exon_ID;
    }
    else{
	if($Exon_Strand eq "+"){ #In case of sense strand, the exon rank order is increasing
	    $Exon_List{$Transcript_ID} = "$Exon_List{$Transcript_ID}\,$Exon_ID";
	}
	else{
	    $Exon_List{$Transcript_ID} = "$Exon_ID\,$Exon_List{$Transcript_ID}";
	}
    }
} 
@Exon_Info = ();
##########################################################################################

#####Step 2: Store the detected exon-exon junction into hash#############################
my %Detected_Exon_Junction = ();
open(IN_2, "$Detected_Exon_Junction");
my @Detected_Exon_Junction = <IN_2>; chomp @Detected_Exon_Junction;
close IN_2;
foreach my $Exon_Junction_Line (@Detected_Exon_Junction){
    my @Exon_Junction_Line_Array = split(/\_/,$Exon_Junction_Line);
    my $Exon_ID_1 = $Exon_Junction_Line_Array[0];
    my $Exon_ID_2 = $Exon_Junction_Line_Array[2];
    my $Junction_ID = "$Exon_ID_1\_$Exon_ID_2";
    $Detected_Exon_Junction{$Junction_ID} = "T";
}
@Detected_Exon_Junction = ();
#########################################################################################

####Step 3: Output the bed data for each transcript ####################################
my $Small_Junction_Block = "";
my $Detected_Junction_Block = "";
open(IN_3, "$Retroposed_Transcript_List");
my @Transcript_List = <IN_3>; chomp @Transcript_List;
close IN_3;

foreach my $Transcript_ID (@Transcript_List){
    $Small_Junction_Block = "";
    $Detected_Junction_Block = "";
    my @Exon_List = split(/\,/,$Exon_List{$Transcript_ID});
    for(my $i=0; $i<scalar(@Exon_List)-1; $i++){
	my $Exon_ID_1 = $Exon_List[$i];
	my $Exon_ID_2 = $Exon_List[$i+1];
	my $Junction_ID = "$Exon_ID_1\_$Exon_ID_2";
	my $Exon_1_Len = Get_Exon_Len($Exon_ID_1);
	my $Exon_2_Len = Get_Exon_Len($Exon_ID_2);
	my $Intron_Len = Get_Intron_Len($Exon_ID_1,$Exon_ID_2);
	
	if($Exon_1_Len<$Exon_Size_Cutoff || $Exon_2_Len<$Exon_Size_Cutoff || $Intron_Len<$Intron_Size_Cutoff){
	    if($Small_Junction_Block eq ""){
		$Small_Junction_Block = "$Exon_ID_1\,$Exon_ID_2";
	    }
	    else{
		$Small_Junction_Block = "$Small_Junction_Block\,$Exon_ID_2";
	    }
	}
	else{
	    if($Small_Junction_Block ne ""){
		my $Small_Junction_Block_Bed = Generate_small_Block_Bed($Small_Junction_Block,$Transcript_ID);
		print OUT "$Small_Junction_Block_Bed\n";
	    }
	    $Small_Junction_Block = "";
	}

	if(defined($Detected_Exon_Junction{$Junction_ID})){
	    if($Detected_Junction_Block eq ""){
		$Detected_Junction_Block = "$Exon_ID_1\,$Exon_ID_2";
	    }
	    else{
		$Detected_Junction_Block = "$Detected_Junction_Block\,$Exon_ID_2";
	    }
	}
	else{
	    if($Detected_Junction_Block ne ""){
                my $Detected_Junction_Block_Bed = Generate_Detected_Block_Bed($Detected_Junction_Block,$Transcript_ID);
                print OUT "$Detected_Junction_Block_Bed\n";
	    }
	    $Detected_Junction_Block = "";
	}
	
	if($i==scalar(@Exon_List)-2){ ##Last pair of exon
	    if($Small_Junction_Block ne ""){
		my $Small_Junction_Block_Bed = Generate_small_Block_Bed($Small_Junction_Block,$Transcript_ID);
                print OUT "$Small_Junction_Block_Bed\n";
            }
	    if($Detected_Junction_Block ne ""){
		my $Detected_Junction_Block_Bed = Generate_Detected_Block_Bed($Detected_Junction_Block,$Transcript_ID);
                print OUT "$Detected_Junction_Block_Bed\n";
            }
	}
    }
} 
close OUT;
###########################################################################################################################


###################Sub functions used in this script ###################################
sub Get_Exon_Len {
    my ($Exon_ID) = @_;
    my $Exon_Len = 0;
    my @Exon_Info_Array = split(/\:/,$Exon_Loc{$Exon_ID});
    my $Exon_Pos1 = $Exon_Info_Array[2];
    my $Exon_Pos2 = $Exon_Info_Array[3];
    $Exon_Len = $Exon_Pos2-$Exon_Pos1+1;
    return $Exon_Len;
}

sub Get_Intron_Len {
    my ($Exon_ID_1, $Exon_ID_2) =  @_;
    my $Intron_Len = 0;
    my @Exon_Info_Array_1 = split(/\:/,$Exon_Loc{$Exon_ID_1});
    my @Exon_Info_Array_2 = split(/\:/,$Exon_Loc{$Exon_ID_2});
    if($Exon_Loc{$Exon_ID_1} =~ /\+/){ #Sense strand
	$Intron_Len = $Exon_Info_Array_2[2]-$Exon_Info_Array_1[3]-1;
    }
    else{
	$Intron_Len = $Exon_Info_Array_1[2]-$Exon_Info_Array_2[3]-1;
    }
    return $Intron_Len;
}

sub Generate_small_Block_Bed { ##Exon junctions with either too small exon or intron
    my ($Small_Junction_Block, $Transcipt_ID) = @_;
    my $Out_Bed = "";
    
    my $Color_Setting = "128,128,128";
    my $Block_Score = 500;
    my @Exon_List = split(/\,/,$Small_Junction_Block);
    my $Block_Chr = "";
    my $chromStart = 0;
    my $chromEnd = 0;
    my $Block_Name = "$Transcipt_ID\_Excluded_Junction";
    my $Block_Strand = "";
    my $Block_Count = scalar(@Exon_List);
    my @Block_Sizes = ();
    my @Block_Starts = ();
    
    my @Exon_1_Array = split(/\:/,$Exon_Loc{$Exon_List[0]});
    $Block_Chr = $Exon_1_Array[0];
    $Block_Strand = $Exon_1_Array[1];
    
    if($Block_Strand eq "+"){ ##Sense strand, exon rank is increasing
	for(my $i=0; $i<scalar(@Exon_List); $i++){
	    my @Exon_Array = split(/\:/,$Exon_Loc{$Exon_List[$i]});
	    my $Exon_Start = $Exon_Array[2];
	    my $Exon_End = $Exon_Array[3];
	    my $Exon_Len = $Exon_End-$Exon_Start+1;
	    if($i==0){
		$chromStart = $Exon_Start;
	    }
	    elsif($i==scalar(@Exon_List)-1){
		$chromEnd = $Exon_End;
	    }
	    @Block_Sizes = (@Block_Sizes,$Exon_Len);
	    @Block_Starts = (@Block_Starts,$Exon_Start-$chromStart);
	}
    }
    elsif($Block_Strand eq "-"){
	for(my $j=scalar(@Exon_List)-1; $j>=0; $j--){
	    my @Exon_Array = split(/\:/,$Exon_Loc{$Exon_List[$j]});
            my $Exon_Start = $Exon_Array[2];
            my $Exon_End = $Exon_Array[3];
            my $Exon_Len = $Exon_End-$Exon_Start+1;
            if($j==scalar(@Exon_List)-1){
                $chromStart = $Exon_Start;
            }
            elsif($j==0){
                $chromEnd = $Exon_End;
            }
            @Block_Sizes = (@Block_Sizes,$Exon_Len);
            @Block_Starts = (@Block_Starts,$Exon_Start-$chromStart);
	}
    }
    
    my $Block_Sizes = join(",",@Block_Sizes);
    my $Block_Starts = join(",",@Block_Starts);
    $Out_Bed = "chr$Block_Chr\t$chromStart\t$chromEnd\t$Block_Name\t$Block_Score\t$Block_Strand\t$chromStart\t$chromEnd\t$Color_Setting\t$Block_Count\t$Block_Sizes\t$Block_Starts";
    return $Out_Bed;
}

sub Generate_Detected_Block_Bed { ##Exon junctions with detected retroposition event
    my ($Detected_Junction_Block, $Transcipt_ID) = @_;
    my $Out_Bed = "";

    my $Color_Setting = "255,0,0";
    my $Block_Score = 1000;
    my @Exon_List = split(/\,/,$Detected_Junction_Block);
    my $Block_Chr = "";
    my $chromStart = 0;
    my $chromEnd = 0;
    my $Block_Name = "$Transcipt_ID\_Retroposed_Junction";
    my $Block_Strand = "";
    my $Block_Count = scalar(@Exon_List);
    my @Block_Sizes = ();
    my @Block_Starts = ();

    my @Exon_1_Array = split(/\:/,$Exon_Loc{$Exon_List[0]});
    $Block_Chr = $Exon_1_Array[0];
    $Block_Strand = $Exon_1_Array[1];

    if($Block_Strand eq "+"){ ##Sense strand, exon rank is increasing
        for(my $i=0; $i<scalar(@Exon_List); $i++){
            my @Exon_Array = split(/\:/,$Exon_Loc{$Exon_List[$i]});
            my $Exon_Start = $Exon_Array[2];
            my $Exon_End = $Exon_Array[3];
            my $Exon_Len = $Exon_End-$Exon_Start+1;
            if($i==0){
                $chromStart = $Exon_Start;
            }
            elsif($i==scalar(@Exon_List)-1){
                $chromEnd = $Exon_End;
            }
            @Block_Sizes = (@Block_Sizes,$Exon_Len);
            @Block_Starts = (@Block_Starts,$Exon_Start-$chromStart);
        }
    }
    elsif($Block_Strand eq "-"){
        for(my $j=scalar(@Exon_List)-1; $j>=0; $j--){
            my @Exon_Array = split(/\:/,$Exon_Loc{$Exon_List[$j]});
            my $Exon_Start = $Exon_Array[2];
            my $Exon_End = $Exon_Array[3];
            my $Exon_Len = $Exon_End-$Exon_Start+1;
            if($j==scalar(@Exon_List)-1){
                $chromStart = $Exon_Start;
            }
            elsif($j==0){
                $chromEnd = $Exon_End;
            }
            @Block_Sizes = (@Block_Sizes,$Exon_Len);
            @Block_Starts = (@Block_Starts,$Exon_Start-$chromStart);
        }
    }

    my $Block_Sizes = join(",",@Block_Sizes);
    my $Block_Starts = join(",",@Block_Starts);
    $Out_Bed = "chr$Block_Chr\t$chromStart\t$chromEnd\t$Block_Name\t$Block_Score\t$Block_Strand\t$chromStart\t$chromEnd\t$Color_Setting\t$Block_Count\t$Block_Sizes\t$Block_Starts";
    return $Out_Bed;
}
