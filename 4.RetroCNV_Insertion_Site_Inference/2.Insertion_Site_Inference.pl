#!/usr/bin/perl -w
use strict;

require HQ_Alignment_Check;

####This script is used to infer the insertion site of gene retroposition based on discordant alignment
##PICARD program is also used in this script.

##############Controlling parameters##############################
my $MapQ_Cutoff = 20; ## Cutoff for mapping quality
my $Score_Diff = 5; ## Cutoff for the difference of best alignment score and second best score
my $Score_Alignment_Ratio = 0; #Cutoff of the ratio between alignment score and aligned length(requiring consideration on the reward/penalty of match/mismatch of alignment setting)
my $Distance_Cutoff = 1000; #1000 bp as the cutoff of the distance that the insertion site should be away from the parental gene/transcript
##################################################################

############Input and output files###############################
my $Retroposed_Transcript_Pos = "./Retroposed_Transcript_Pos.txt";
my $Discordant_Alignment_Read = "Discordant_Alignment_Read.id";
my $Discordant_Alignment = "Discordant_Alignment_Read.sam";
my $Reference_Genome_Alignment = "../3.RetroCNV_Gene_Calling/Target_Seq_Extraction_100bp_30X_Reference_Genome_Aligned_Sorted_nodup.bam";
my $Discordant_Alignment_Same_Strand_Result = "Discordant_Alignment_Same_Strand.result";
my $Discordant_Alignment_Diff_Strand_Result = "Discordant_Alignment_Diff_Strand.result";
open(OUT_1, ">$Discordant_Alignment_Same_Strand_Result");
open(OUT_2, ">$Discordant_Alignment_Diff_Strand_Result");
################################################################

#########Step 1: Generate the full alignment based on the above detected discordant alignment reads#########################
my $Cmd = "java -jar \$PICARD FilterSamReads INPUT=$Reference_Genome_Alignment FILTER=includeReadList READ_LIST_FILE=$Discordant_Alignment_Read OUTPUT=$Discordant_Alignment";
print "$Cmd\n";
system($Cmd);
############################################################################################################################

#########Step 2: Store HQ alignment into hash #########################
my @Correct_Flag = ("65","129","81","161","97","145","113","177"); ##Correct flags for overall alignment of both read pair mapping to the parental gene and new flanking region
my %Correct_Flag = ();
foreach my $Flag_ID (@Correct_Flag){
    $Correct_Flag{$Flag_ID} = 1;
}

my %HQ_Alignment_Data = ();
my %Final_HQ_Alignment_Data = ();
open(IN_1, "$Discordant_Alignment");
my @Discordant_Alignment = <IN_1>; chomp @Discordant_Alignment;
close IN_1;
foreach my $Discordant_Alignment_Line (@Discordant_Alignment){
    if(!($Discordant_Alignment_Line =~ /^@/)){ #Not the header line
	my @Discordant_Alignment_Line_Array = split(/\t/,$Discordant_Alignment_Line);
	my $Read_ID = $Discordant_Alignment_Line_Array[0];
	my $Alignment_Flag = $Discordant_Alignment_Line_Array[1];
	my $Alignment_Chr = $Discordant_Alignment_Line_Array[2];
	my $Alignment_Pos = $Discordant_Alignment_Line_Array[3];
	my $Alignment_CIARG = $Discordant_Alignment_Line_Array[5];
	my $Read_Len = length($Discordant_Alignment_Line_Array[9]);
	my $Clipped_Len = HQ_Alignment_Check::Get_Soft_Clipping_Len($Alignment_CIARG);
	my $Aligned_Len = $Read_Len - $Clipped_Len;
      
	my $HQ_Alignment_Flag = HQ_Alignment_Check::HQ_Alignment_Check($Discordant_Alignment_Line,$MapQ_Cutoff,$Score_Diff,$Score_Alignment_Ratio);
	if(defined($Correct_Flag{$Alignment_Flag}) && $HQ_Alignment_Flag eq "T"){
	    my $Alignment_Info = "MapFlag$Alignment_Flag\:$Alignment_Chr\:$Alignment_Pos\:$Aligned_Len";
	    if(!defined($HQ_Alignment_Data{$Read_ID})){
		$HQ_Alignment_Data{$Read_ID} = $Alignment_Info;
	    }
	    else{ #Only if both reads can be uniquely HQ mapped
		$Final_HQ_Alignment_Data{$Read_ID} = "$HQ_Alignment_Data{$Read_ID}\t$Alignment_Info";
	    }
	}
    }
}
@Discordant_Alignment = ();
%HQ_Alignment_Data = ();
#unlink "$Discordant_Alignment";
###############################################################################################################

#######Step 3: Get the insertion site for each retroposed transcript individually #########################
my @Left_Insertion_Flag = ("MapFlag97","MapFlag161","MapFlag65","MapFlag129");
my @Right_Insertion_Flag = ("MapFlag81","MapFlag145","MapFlag113","MapFlag177");
my %Left_Insertion_Flag = ();
my %Right_Insertion_Flag = ();
foreach my $Flag_ID (@Left_Insertion_Flag){
    $Left_Insertion_Flag{$Flag_ID} = 1;
}
foreach my $Flag_ID (@Right_Insertion_Flag){
    $Right_Insertion_Flag{$Flag_ID} = 1;
}

open(IN_2, "$Retroposed_Transcript_Pos");
my @Retroposed_Transcript = <IN_2>; chomp @Retroposed_Transcript;
close IN_2;
my $D_C = "|"; #Delimiter character
foreach my $Retroposed_Transcript_Line (@Retroposed_Transcript){
    my $Transcript_ID = substr($Retroposed_Transcript_Line,0,index($Retroposed_Transcript_Line,"\t"));
    my @Transcript_Loc = split(/\:/,substr($Retroposed_Transcript_Line,index($Retroposed_Transcript_Line,"\t")+1));
    my $Transcript_Chr = $Transcript_Loc[0];
    my $Transcript_Start_Pos = $Transcript_Loc[2];
    my $Transcript_End_Pos = $Transcript_Loc[3];
    my $Out_Str = "$Retroposed_Transcript_Line";
    my @Target_Support_Read_Same_Strand = (); ## For the read within the gene region (same strand for retrocopy and parental gene)
    my @Mate_Support_Read_Same_Strand = (); ## Mate reads that is out of gene region (Same strand)
    my @Target_Support_Read_Diff_Strand = (); ## For the read within the gene region (Diff strand for retrocopy and parental gene)
    my @Mate_Support_Read_Diff_Strand = (); ## Mate reads that is out of gene region (Diff strand)

    foreach my $Read_ID (keys %Final_HQ_Alignment_Data){
	my @Alignment_Combined = split(/\t/,$Final_HQ_Alignment_Data{$Read_ID});
	my @Alignment_Info_1 = split(/\:/,$Alignment_Combined[0]);
	my @Alignment_Info_2 = split(/\:/,$Alignment_Combined[1]);

	my $Alignment_Flag_1 = $Alignment_Info_1[0];
	my $Read_Chr_1 = $Alignment_Info_1[1];
	my $Read_Pos_1 = $Alignment_Info_1[2];
	my $Mapped_Len_1 = $Alignment_Info_1[3];
	my $Alignment_Flag_2 = $Alignment_Info_2[0];
	my $Read_Chr_2 = $Alignment_Info_2[1];
	my $Read_Pos_2 = $Alignment_Info_2[2];
	my $Mapped_Len_2 = $Alignment_Info_2[3];

	my $Out_Transcript_Flag_1 = Whether_Outside_Transcript($Read_Chr_1,$Read_Pos_1,$Transcript_Chr,$Transcript_Start_Pos,$Transcript_End_Pos);
	my $Out_Transcript_Flag_2 = Whether_Outside_Transcript($Read_Chr_2,$Read_Pos_2,$Transcript_Chr,$Transcript_Start_Pos,$Transcript_End_Pos);

	my $Target_Read_Map_Pos = "";
	my $Mate_Read_Map_Pos = "";
	my $New_Map_Pos = 0; ##In case of left side insertion, the pos needed to be adjusted by adding the mapping length
	if($Out_Transcript_Flag_1 eq "F" && $Out_Transcript_Flag_2 eq "T"){
	    if($Alignment_Flag_1 eq "MapFlag145" || $Alignment_Flag_1 eq "MapFlag81"){
		$New_Map_Pos = $Read_Pos_2 + $Mapped_Len_2;
		$Target_Read_Map_Pos = "$Read_ID$D_C"."L$D_C$Alignment_Flag_1$D_C$Read_Chr_1$D_C$Read_Pos_1";
		$Mate_Read_Map_Pos = "$Read_ID$D_C"."L$D_C$Alignment_Flag_2$D_C$Read_Chr_2$D_C$New_Map_Pos";
		@Target_Support_Read_Same_Strand = (@Target_Support_Read_Same_Strand,$Target_Read_Map_Pos);
		@Mate_Support_Read_Same_Strand = (@Mate_Support_Read_Same_Strand,$Mate_Read_Map_Pos);
	    }
	    elsif($Alignment_Flag_1 eq "MapFlag97" || $Alignment_Flag_1 eq "MapFlag161"){
		$Target_Read_Map_Pos = "$Read_ID$D_C"."R$D_C$Alignment_Flag_1$D_C$Read_Chr_1$D_C$Read_Pos_1";
		$Mate_Read_Map_Pos = "$Read_ID$D_C"."R$D_C$Alignment_Flag_2$D_C$Read_Chr_2$D_C$Read_Pos_2";
		@Target_Support_Read_Same_Strand = (@Target_Support_Read_Same_Strand,$Target_Read_Map_Pos);
		@Mate_Support_Read_Same_Strand = (@Mate_Support_Read_Same_Strand,$Mate_Read_Map_Pos);
	    }
	    elsif($Alignment_Flag_1 eq "MapFlag65" || $Alignment_Flag_1 eq "MapFlag129"){
		$New_Map_Pos = $Read_Pos_2 + $Mapped_Len_2;
		$Target_Read_Map_Pos = "$Read_ID$D_C"."L$D_C$Alignment_Flag_1$D_C$Read_Chr_1$D_C$Read_Pos_1";
		$Mate_Read_Map_Pos = "$Read_ID$D_C"."L$D_C$Alignment_Flag_2$D_C$Read_Chr_2$D_C$New_Map_Pos";
		@Target_Support_Read_Diff_Strand = (@Target_Support_Read_Diff_Strand,$Target_Read_Map_Pos);
		@Mate_Support_Read_Diff_Strand = (@Mate_Support_Read_Diff_Strand,$Mate_Read_Map_Pos);
	    }
	    elsif($Alignment_Flag_1 eq "MapFlag113" || $Alignment_Flag_1 eq "MapFlag177"){
		$Target_Read_Map_Pos = "$Read_ID$D_C"."R$D_C$Alignment_Flag_1$D_C$Read_Chr_1$D_C$Read_Pos_1";
		$Mate_Read_Map_Pos = "$Read_ID$D_C"."R$D_C$Alignment_Flag_2$D_C$Read_Chr_2$D_C$Read_Pos_2";
		@Target_Support_Read_Diff_Strand = (@Target_Support_Read_Diff_Strand,$Target_Read_Map_Pos);
		@Mate_Support_Read_Diff_Strand = (@Mate_Support_Read_Diff_Strand,$Mate_Read_Map_Pos);
	    }
	}
	elsif($Out_Transcript_Flag_1 eq "T" && $Out_Transcript_Flag_2 eq "F"){
	    if($Alignment_Flag_2 eq "MapFlag145" || $Alignment_Flag_2 eq "MapFlag81"){
		$New_Map_Pos = $Read_Pos_1 + $Mapped_Len_1;
		$Target_Read_Map_Pos = "$Read_ID$D_C"."L$D_C$Alignment_Flag_2$D_C$Read_Chr_2$D_C$Read_Pos_2";
		$Mate_Read_Map_Pos = "$Read_ID$D_C"."L$D_C$Alignment_Flag_1$D_C$Read_Chr_1$D_C$New_Map_Pos";
		@Target_Support_Read_Same_Strand = (@Target_Support_Read_Same_Strand,$Target_Read_Map_Pos);
		@Mate_Support_Read_Same_Strand = (@Mate_Support_Read_Same_Strand,$Mate_Read_Map_Pos);
	    }
	    elsif($Alignment_Flag_2 eq "MapFlag97" || $Alignment_Flag_2 eq "MapFlag161"){
		$Target_Read_Map_Pos = "$Read_ID$D_C"."R$D_C$Alignment_Flag_2$D_C$Read_Chr_2$D_C$Read_Pos_2";
		$Mate_Read_Map_Pos = "$Read_ID$D_C"."R$D_C$Alignment_Flag_1$D_C$Read_Chr_1$D_C$Read_Pos_1";
		@Target_Support_Read_Same_Strand = (@Target_Support_Read_Same_Strand,$Target_Read_Map_Pos);
		@Mate_Support_Read_Same_Strand = (@Mate_Support_Read_Same_Strand,$Mate_Read_Map_Pos);
	    }
	    elsif($Alignment_Flag_2 eq "MapFlag65" || $Alignment_Flag_2 eq "MapFlag129"){
		$New_Map_Pos = $Read_Pos_1 + $Mapped_Len_1;
		$Target_Read_Map_Pos = "$Read_ID$D_C"."L$D_C$Alignment_Flag_2$D_C$Read_Chr_2$D_C$Read_Pos_2";
		$Mate_Read_Map_Pos = "$Read_ID$D_C"."L$D_C$Alignment_Flag_1$D_C$Read_Chr_1$D_C$New_Map_Pos";
		@Target_Support_Read_Diff_Strand = (@Target_Support_Read_Diff_Strand,$Target_Read_Map_Pos);
		@Mate_Support_Read_Diff_Strand = (@Mate_Support_Read_Diff_Strand,$Mate_Read_Map_Pos);
	    }
	    elsif($Alignment_Flag_2 eq "MapFlag113" || $Alignment_Flag_2 eq "MapFlag177"){
		$Target_Read_Map_Pos = "$Read_ID$D_C"."R$D_C$Alignment_Flag_2$D_C$Read_Chr_2$D_C$Read_Pos_2";
		$Mate_Read_Map_Pos = "$Read_ID$D_C"."R$D_C$Alignment_Flag_1$D_C$Read_Chr_1$D_C$Read_Pos_1";
		@Target_Support_Read_Diff_Strand = (@Target_Support_Read_Diff_Strand,$Target_Read_Map_Pos);
		@Mate_Support_Read_Diff_Strand = (@Mate_Support_Read_Diff_Strand,$Mate_Read_Map_Pos);
	    }
	}
#	print "$Target_Read_Map_Pos\n";
    }
    my $Str_Target_Support_Read_Same_Strand = join(";",@Target_Support_Read_Same_Strand);
    my $Str_Mate_Support_Read_Same_Strand = join(";",@Mate_Support_Read_Same_Strand);
    my $Str_Target_Support_Read_Diff_Strand = join(";",@Target_Support_Read_Diff_Strand);
    my $Str_Mate_Support_Read_Diff_Strand = join(";",@Mate_Support_Read_Diff_Strand);
    print OUT_1 "$Out_Str\t$Str_Target_Support_Read_Same_Strand\t$Str_Mate_Support_Read_Same_Strand\n";
    print OUT_2 "$Out_Str\t$Str_Target_Support_Read_Diff_Strand\t$Str_Mate_Support_Read_Diff_Strand\n";
}
close OUT_1;
close OUT_2;

###################Sub functions used in this script ##########################################################
sub Whether_Outside_Transcript { ##Whether the pos is outside the original parental gene
    my ($Alignment_Chr,$Alignment_Pos,$Transcript_Chr,$Transcript_Start,$Transcript_End) = @_;
    my $Outside_Flag = "F";
    if("$Alignment_Chr" eq "$Transcript_Chr"){ #on the same chr
        if(($Alignment_Pos-$Transcript_Start+$Distance_Cutoff)*($Alignment_Pos-$Transcript_End-$Distance_Cutoff)>0){
            $Outside_Flag = "T";
        }
    }
    else{
        $Outside_Flag = "T";
    }
    return $Outside_Flag;
}
