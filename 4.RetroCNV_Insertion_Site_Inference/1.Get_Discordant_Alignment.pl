#!/usr/bin/perl -w
use strict;

require HQ_Alignment_Check;

####This script is used to get the discordant alignment, with one read uniquely mapping to the parental gene, and the other discordantly mapped####################
##samtools program is also used in this script.

##############Controlling parameters##############################
my $MapQ_Cutoff = 20; ## Cutoff for mapping quality
my $Score_Diff = 5; ## Cutoff for the difference of best alignment score and second best score
my $Score_Alignment_Ratio = 0; #Cutoff of the ratio between alignment score and aligned length(requiring consideration on the reward/penalty of match/mismatch of alignment setting)
my $Insert_Size_Cutoff = 1000; #1000 bp as the cutoff to define discordant alignment on the same chromosome
##################################################################

############Input and output files###############################
my $Retroposed_Transcript = "../3.RetroCNV_Gene_Calling/Retroposed_Transcript.list";
my $Exon_Info = "../1.Create_Exon_Junction_Lib/Mus_musculus.GRCm38.87.exon.info";
my $Exon_Junction_Bed = "Exon_Junction_Detected.bed"; ##The positions of exons for the transcript(s) that are detected to be retroposed
my $Discordant_Alignment_Read = "Discordant_Alignment_Read.id";
my $Reference_Genome_Alignment = "../3.RetroCNV_Gene_Calling/Target_Seq_Extraction_100bp_30X_Reference_Genome_Aligned_Sorted_nodup.bam";
my $Retroposed_Transcript_Pos = "Retroposed_Transcript_Pos.txt";
open(OUT_1, ">$Exon_Junction_Bed");
open(OUT_2, ">$Retroposed_Transcript_Pos");
open(OUT_3, ">$Discordant_Alignment_Read");
################################################################

#########Step 1: Generate bed file for the exon positions of detected retroposed transcript(s)#########################
my %Retroposed_Transcript = ();
open(IN_1, "$Retroposed_Transcript");
my @Retroposed_Transcript = <IN_1>; chomp @Retroposed_Transcript;
close IN_1;
foreach my $Retroposed_Transcript_ID (@Retroposed_Transcript){
    $Retroposed_Transcript{$Retroposed_Transcript_ID} = 1;
}
@Retroposed_Transcript = ();

my %Transcript_Chr_Strand = ();
my %Transcript_Start_Pos = ();
my %Transcript_End_Pos = ();
my %Exon_Uniq = ();
open(IN_2, "$Exon_Info");
my @Exon_Info = <IN_2>; chomp @Exon_Info;
close IN_2;
foreach my $Exon_Info_Line (@Exon_Info){
    my @Exon_Info_Line_Array = split(/\t/,$Exon_Info_Line);
    my $Exon_ID = $Exon_Info_Line_Array[0];
    my $Exon_Chr = $Exon_Info_Line_Array[1];
    my $Exon_Strand = $Exon_Info_Line_Array[2];
    my $Exon_Start = $Exon_Info_Line_Array[3];
    my $Exon_End = $Exon_Info_Line_Array[4];
    my $Transcript_ID = $Exon_Info_Line_Array[5];
    if(defined($Retroposed_Transcript{$Transcript_ID})){
	if(!defined($Exon_Uniq{$Exon_ID})){
	    print OUT_1 "$Exon_Chr\t$Exon_Start\t$Exon_End\n";
	    $Exon_Uniq{$Exon_ID} = 1;
	}
	my $Chr_Strand = "$Exon_Chr\:$Exon_Strand";
	$Transcript_Chr_Strand{$Transcript_ID} = $Chr_Strand;
	
	if(!defined($Transcript_Start_Pos{$Transcript_ID})){
	    $Transcript_Start_Pos{$Transcript_ID} = $Exon_Start;
	}
	else{
	    if($Exon_Start < $Transcript_Start_Pos{$Transcript_ID}){
		$Transcript_Start_Pos{$Transcript_ID} = $Exon_Start;
	    }
	}
	
	if(!defined($Transcript_End_Pos{$Transcript_ID})){
            $Transcript_End_Pos{$Transcript_ID} = $Exon_End;
        }
        else{
            if($Exon_End > $Transcript_End_Pos{$Transcript_ID}){
                $Transcript_End_Pos{$Transcript_ID} = $Exon_End;
            }
        }
    }
}

for my $Transcript_ID (keys %Transcript_Chr_Strand){
    print OUT_2 "$Transcript_ID\t$Transcript_Chr_Strand{$Transcript_ID}\:$Transcript_Start_Pos{$Transcript_ID}\:$Transcript_End_Pos{$Transcript_ID}\n";
}
close OUT_1;
close OUT_2;
@Exon_Info = ();
%Retroposed_Transcript = ();
%Exon_Uniq = ();
###########################################################################################################

#########Step 2: Get "good" HQ alignment result ##########################################################
#1) Selecte the alignment data within the above bed region
my $Seletected_Alignment = "Selected_Alignment.sam";
my $Cmd = "samtools view -L $Exon_Junction_Bed $Reference_Genome_Alignment >$Seletected_Alignment";
print "$Cmd\n";
system($Cmd);

#2) Get the HQ and discordant alignment results
my @Correct_Flag = ("65","129","81","161","97","145","113","177");
my %Correct_Flag = (); ##These flags indicate the unique alignment with correct orientation, but wrong insert size
foreach my $Flag_ID (@Correct_Flag){
    $Correct_Flag{$Flag_ID} = 1;
}

my %Uniq_Discordant_Read = ();
open(IN_3, "$Seletected_Alignment");
my @Selected_Alignment = <IN_3>; chomp @Selected_Alignment;
close IN_3;
foreach my $Alignment_Line (@Selected_Alignment){
    my $HQ_Flag = HQ_Alignment_Check::HQ_Alignment_Check($Alignment_Line,$MapQ_Cutoff,$Score_Diff,$Score_Alignment_Ratio);
    if($HQ_Flag eq "T"){
	my @Alignment_Line_Array = split(/\t/,$Alignment_Line);
	my $Read_ID = $Alignment_Line_Array[0];
	my $Map_Flag = $Alignment_Line_Array[1];
	my $Mate_Chr = $Alignment_Line_Array[6]; #alignment chr of the mate read
	my $Insert_Size = abs($Alignment_Line_Array[8]); #Inferred insert size
	if(defined($Correct_Flag{$Map_Flag})){
	    if(($Mate_Chr ne "=" && $Mate_Chr ne "*") || ($Mate_Chr eq "=" && $Insert_Size >= $Insert_Size_Cutoff)){
		if(!defined($Uniq_Discordant_Read{$Read_ID})){
		    print OUT_3 "$Read_ID\n";
		    $Uniq_Discordant_Read{$Read_ID} = 1;
		}
	    }
	}
    }
}
close OUT_3;
@Selected_Alignment = ();
unlink "$Exon_Junction_Bed";
unlink "$Seletected_Alignment";
#############################################################################################################
