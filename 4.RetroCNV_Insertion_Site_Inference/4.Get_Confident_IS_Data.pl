#!/usr/bin/perl -w
use strict;

#####This script is used to get insertion site estimate with high confidence 
#High confidence: ">=2 supporinting evidence for both Left and right side" OR ">=4 in total supporting evidences"

#######Controlling parameters ####################################################################################
my $Num_Evi_Left_Cutoff = 2;
my $Num_Evi_Right_Cutoff = 2;
my $Num_Evi_Total_Cutoff = 4;
##################################################################################################################

############# process on the Insertion site data from last step ##################################################
my $Collapsed_IS_Data = "Insertion_Collapse_Result.txt";
my $Filtered_IS_Data = "Filtered_Insertion_Collapse_Result.txt";
open(IN, "$Collapsed_IS_Data");
open(OUT, ">$Filtered_IS_Data");
print OUT "Transcript_ID\tTranscript_Loc\tInsertion_Site_Estimate\n";
my @IS_Data = <IN>; chomp @IS_Data;
close IN;

foreach my $IS_Data_Line (@IS_Data){
    my $Left_Supp = 0; ##Number of supporting evidence for the left side
    my $Right_Supp = 0; ##Number of supporting evidence for the right side
    my @IS_Data_Line_Array = split(/\t/,$IS_Data_Line);
    if($IS_Data_Line_Array[4] =~ /\-/){ ##There is IS reported there
	my $Transcript_ID = $IS_Data_Line_Array[0];
	my $Transcript_Loc = $IS_Data_Line_Array[1];
	my @Transcript_Loc_Array = split(/\:/,$Transcript_Loc);
	my $Transcript_Strand = $Transcript_Loc_Array[1];
	my @Evidence_Data = split(/\;/,$IS_Data_Line_Array[2]);
	my $IS_Loc = $IS_Data_Line_Array[4];
	my @IS_Loc_Array = split(/\:/,$IS_Loc);
	my $IS_Orientation = $IS_Data_Line_Array[5];
	my $IS_Strand = "";

	if($IS_Orientation eq "Same_Strand"){
	    $IS_Strand = $Transcript_Strand;
	}
	else{
	    if($Transcript_Strand eq "+"){
		$IS_Strand = "-";
	    }
	    else{
		$IS_Strand = "+";
	    }
	}
	my $IS_Loc_New = "$IS_Loc_Array[0]\:$IS_Strand\:$IS_Loc_Array[1]";

	foreach my $Evidence_Data_Line (@Evidence_Data){
	    if($Evidence_Data_Line =~ /\|L\|/){
		$Left_Supp ++;
	    }
	    elsif($Evidence_Data_Line =~ /\|R\|/){
		$Right_Supp ++;
	    }
	}
	if(($Left_Supp>=$Num_Evi_Left_Cutoff && $Right_Supp>=$Num_Evi_Right_Cutoff) || ($Left_Supp+$Right_Supp>=$Num_Evi_Total_Cutoff)){
	    print OUT "$Transcript_ID\t$Transcript_Loc\t$IS_Loc_New\n";
	}
    }
}
close OUT;
#####################################################################################################################
