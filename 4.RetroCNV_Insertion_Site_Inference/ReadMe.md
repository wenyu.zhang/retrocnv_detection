## This document shows the process to infer the insertion site of gene retroposition

Required programs:
* samtools
* PICARD

### Step 1: Get the discordant alignment data, with one read uniquely mapped to the exons of parental gene, while the mate pair mapped discordantly 
> perl 1.Get_Discordant_Alignment.pl

### Step 2: Get the possible insertion sites, based on the above discordant alignment results. 
> perl 2.Insertion_Site_Inference.pl

### Step 3: Collapse the inferred insertion sites (1000bp as clustering distance) from last step
> perl 3.Collapse_Insertion_Site.pl

### Step 4: Get high quality insertion site estimate: with >=2 supporting reads on both sides, or >=4 supporting reads on either side
> perl 4.Get_Confident_IS_Data.pl

* Actual insertion site for simulated retroCNV: 8(+):114193599
* Estimated insertion site from the computational pipeline: 8(+):114193263-114193958