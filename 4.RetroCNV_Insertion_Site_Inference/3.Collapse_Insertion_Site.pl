#!/usr/bin/perl -w
use strict;
use Statistics::Descriptive;

######This script is used to collapse and summarize the insertion position information based on the discordant algined paried end reads

##############Controlling parameters##############################
my $Collapse_Size_Cutoff = 1000; ##The pos difference of the insertion sites to be clustered
my $Support_Evi_Size = 2; ##The minimum number of reads to support one insertion sites
##################################################################

############Input and output files###############################
my @Insertion_Chr_Patterns = ("Same_Strand","Diff_Strand");
my $Insertion_Collapse_Result = "Insertion_Collapse_Result.txt";
open(OUT, ">$Insertion_Collapse_Result");
################################################################

########### Analysis on the origial IS detection results #######################
my $D_C = "|"; #Delimiter character
foreach my $Insertion_Chr_Pattern (@Insertion_Chr_Patterns){	
    open(IN, "Discordant_Alignment\_$Insertion_Chr_Pattern\.result");
    my @IS_Data = <IN>; chomp @IS_Data;
    close IN;

    foreach my $Evi_Line (@IS_Data){
	my @Evi_Line_Array = split(/\t/,$Evi_Line);
	my $Parental_Transcript_ID = $Evi_Line_Array[0];
	my $Parental_Transcript_Pos = $Evi_Line_Array[1];
	my @Anchor_Alignment = ();
	my @Retro_Alignment = ();
	if(scalar(@Evi_Line_Array)<3){ ##In case there is no evidence to get IS
	    #print OUT "$Parental_Transcript_ID\t$Parental_Transcript_Pos\n";
	    next;
	}
	else{
	    @Anchor_Alignment = split(/;/,$Evi_Line_Array[2]); ##Anchor alignment to the parental transcript
	    @Retro_Alignment = split(/;/,$Evi_Line_Array[3]); ##New insertion site of retroCNV
	}
	
	if(scalar(@Anchor_Alignment)<$Support_Evi_Size){ ##In case there is no evidence to get IS or the supporting reads<3
	    #print OUT "$Parental_Transcript_ID\t$Parental_Transcript_Pos\n";
	    next;
	}
	else{
	    ##Store the mapping pos to the host gene of each read in hash
	    my %Anchor_Alignment = ();
	    foreach my $Anchor_Alignment_Line (@Anchor_Alignment){
		my $Read_ID = substr($Anchor_Alignment_Line,0,index($Anchor_Alignment_Line,"$D_C"));
		my $Mapping_Pos = substr($Anchor_Alignment_Line,index($Anchor_Alignment_Line,"$D_C")+1);
		$Anchor_Alignment{$Read_ID} = $Mapping_Pos;
	    }
	    
	    ##Collapse the alignment of insertion sites of retroCNV (Take the alignment within 1000bp range into one cluster)
	    my @Retro_Alignment_Collapsed = Collapse_Retro_Alignment(@Retro_Alignment);
	    if(scalar(@Retro_Alignment_Collapsed)==0){ ##No supporting evidence can be clustered
		#print OUT "$Parental_Transcript_ID\t$Parental_Transcript_Pos\n";
		next;
	    }
	    else{ ##At least one cluster can be found
		foreach my $Retro_Alignment_Collapsed_Line (@Retro_Alignment_Collapsed){
		    ## Get alignment for host gene, sort based on the host gene alignment position and then ouput the IS alignment info accordingly.
		    my %Retro_Alignment_Collapsed = ();
		    my @Retro_Anchor_Alignment = (); ##Array of hashes to store the anchor alignment of this retro IS alignment
		    my @Retro_Alignment_Collapsed_Array = split(/;/,$Retro_Alignment_Collapsed_Line);
		    my $Mapping_Chr = "";
		    my @Mapping_Coor_Array = ();
		    for(my $i=0; $i<@Retro_Alignment_Collapsed_Array; $i++){
			my @Temp_Array = split(/\|/,$Retro_Alignment_Collapsed_Array[$i]);
			my $Read_ID = $Temp_Array[0];
			my $Insertion_Sideness = $Temp_Array[1];
			my $Alignment_Flag = $Temp_Array[2];
			$Mapping_Chr = $Temp_Array[3];
			my $Mapping_Coor = $Temp_Array[4];
			@Mapping_Coor_Array = (@Mapping_Coor_Array,$Mapping_Coor);
			$Retro_Alignment_Collapsed{$Read_ID} = "$Insertion_Sideness$D_C$Alignment_Flag$D_C$Mapping_Chr$D_C$Mapping_Coor";
			
			$Retro_Anchor_Alignment[$i]{ID} = $Read_ID;
			my @Retro_Anchor_Alignment_Array = split(/\|/,$Anchor_Alignment{$Read_ID});
			$Retro_Anchor_Alignment[$i]{Sideness} = $Retro_Anchor_Alignment_Array[0];
			$Retro_Anchor_Alignment[$i]{Flag} = $Retro_Anchor_Alignment_Array[1];
			$Retro_Anchor_Alignment[$i]{Chr} = $Retro_Anchor_Alignment_Array[2];
			$Retro_Anchor_Alignment[$i]{Coor} = $Retro_Anchor_Alignment_Array[3];
		    }
		    ##Sort the $Retro_Anchor_Alignment based on the alignment coordinate
		    my @Indices = sort { $Retro_Anchor_Alignment[$a]{Coor} <=> $Retro_Anchor_Alignment[$b]{Coor} } 0 .. $#Retro_Anchor_Alignment;
		    my $Anchor_Alignment_Str = ""; ## to output
		    my $Retro_Anchor_Alignment_Str = ""; ## To output
		    foreach my $New_Index (@Indices){
			$Anchor_Alignment_Str = "$Anchor_Alignment_Str$Retro_Anchor_Alignment[$New_Index]{ID}$D_C$Retro_Anchor_Alignment[$New_Index]{Sideness}$D_C$Retro_Anchor_Alignment[$New_Index]{Flag}$D_C$Retro_Anchor_Alignment[$New_Index]{Chr}$D_C$Retro_Anchor_Alignment[$New_Index]{Coor}\;";
			$Retro_Anchor_Alignment_Str = "$Retro_Anchor_Alignment_Str$Retro_Anchor_Alignment[$New_Index]{ID}$D_C$Retro_Alignment_Collapsed{$Retro_Anchor_Alignment[$New_Index]{ID}}\;";
		    }
		    chop $Anchor_Alignment_Str;
		    chop $Retro_Anchor_Alignment_Str;
		    
		    ##Get the estimate range of insertion coordinate
		    my $stat = Statistics::Descriptive::Full->new();
		    $stat->add_data(@Mapping_Coor_Array);
		    my $Mapping_Coor_Min = $stat->min();
		    my $Mapping_Coor_Max = $stat->max();
		    print OUT "$Parental_Transcript_ID\t$Parental_Transcript_Pos\t$Anchor_Alignment_Str\t$Retro_Anchor_Alignment_Str\t$Mapping_Chr\:$Mapping_Coor_Min\-$Mapping_Coor_Max\t$Insertion_Chr_Pattern\n";
		}	    
	    }
	}
    }
}
close OUT;
###################################################################################################################################

###############################Sub functions used in this script ##################################################################
sub Collapse_Retro_Alignment {
    my (@Retro_Alignment) = @_;
    my @Retro_Alignment_Collapsed = ();
    for(my $i=0;$i<scalar(@Retro_Alignment)-1;$i++){
	for(my $j=$i+1;$j<scalar(@Retro_Alignment);$j++){
	    if(($Retro_Alignment[$i] ne "") && ($Retro_Alignment[$j] ne "")){
		my @Alignment_Array_1 = split(/;/,$Retro_Alignment[$i]);
		my @Pos_Array= (); ## Array to store all the pos
		my $Chr_ID_1 = ""; ##Chr
		foreach my $Alignment_Line (@Alignment_Array_1){
		    my @Alignment_Info = split(/\|/,$Alignment_Line);
		    $Chr_ID_1 = $Alignment_Info[3];
		    @Pos_Array = (@Pos_Array,$Alignment_Info[4]);
		}
		my $stat = Statistics::Descriptive::Full->new();
		$stat->add_data(@Pos_Array);
		my $Ave_Pos = $stat->mean(); ##The middle point pos for the data in current cluster
		my @Alignment_Array_2 = split(/\|/,$Retro_Alignment[$j]);
		my $Chr_ID_2 = $Alignment_Array_2[3];
		my $Pos_2 = $Alignment_Array_2[4];
		if(("$Chr_ID_1" eq "$Chr_ID_2") && (abs($Ave_Pos-$Pos_2)<=$Collapse_Size_Cutoff)){ ## Same chromosome, and the difference <= 1000bp from different discordant reads
		    $Retro_Alignment[$i] = "$Retro_Alignment[$i]\;$Retro_Alignment[$j]";
		    $Retro_Alignment[$j] = "";
		}
	    }
	}
    }
    
    foreach my $New_Retro_Line (@Retro_Alignment){
	my @Support_Evi_Data = split(/;/,$New_Retro_Line);
	if (scalar(@Support_Evi_Data)>=$Support_Evi_Size){ ## To control the minimum number of supporting evidence
	    @Retro_Alignment_Collapsed = (@Retro_Alignment_Collapsed,$New_Retro_Line);
	}
    }
    return @Retro_Alignment_Collapsed;
}
