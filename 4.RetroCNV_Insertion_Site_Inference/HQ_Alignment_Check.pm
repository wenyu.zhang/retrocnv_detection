package HQ_Alignment_Check;

use warnings;
use strict;

sub Get_Best_Alignment_Score{
    my (@Data_Array) = @_;
    my $Best_Score = 0;
    foreach my $Field (@Data_Array){
        if($Field =~ /AS\:i\:/){
            my @Temp_Str = split(/\:/,$Field);
            $Best_Score = $Temp_Str[2];
            last;
        }
    }
    return $Best_Score;
}

sub Get_Second_Best_Alignment_Score{
    my (@Data_Array) = @_;
    my $Second_Score = 0;
    foreach my $Field (@Data_Array){
        if($Field =~ /XS\:i\:/){
            my @Temp_Str = split(/\:/,$Field);
            $Second_Score = $Temp_Str[2];
            last;
        }
    }
    return $Second_Score;
}

sub Get_Soft_Clipping_Len {
    my ($CIAGR) = @_;
    my $Clipped_Len = 0;
    while($CIAGR =~ /S/){
        my $P = index($CIAGR,"S");
        my $Len = "";
        my @Temp_Array = split(//,$CIAGR);
        for(my $i=$P-1; $i>=0; $i--){
            if($Temp_Array[$i] =~ /\d/){
                $Len = "$Temp_Array[$i]$Len";
            }
            else{
                last;
            }
        }
        $Clipped_Len = $Clipped_Len + $Len;
        $CIAGR = substr($CIAGR,$P+1,);
    }
    return $Clipped_Len;
}

sub HQ_Alignment_Check {
    my ($Alignment_Line,$MapQ_Cutoff,$Score_Diff_Cutoff,$Score_Alignment_Ratio) = @_;
    my $HQ_Flag = "F";
    my $MapQ = 0;
    my $Best_Score = 0; ##AS
    my $Second_Score = 0; ##XS
    my $Score_Diff = 0; #AS-XS
    my $Read_Len = 0; ##Total length of the read
    my $Clipped_Len = 0; ##Only consider soft-clipping, as hard-clipping can be from read seq.
    my $Aligned_Len = 0; ##Aligned read len = Read length - clipped fragment length

    my @Alignment_Array = split(/\t/,$Alignment_Line);
    $MapQ = $Alignment_Array[4];

    $Best_Score = Get_Best_Alignment_Score(@Alignment_Array);
    if($Alignment_Line =~ /XS\:/){ ## In case that second best alignment score exists
        $Second_Score = Get_Second_Best_Alignment_Score(@Alignment_Array);
    }
    $Score_Diff = $Best_Score-$Second_Score;

    $Read_Len = length($Alignment_Array[9]);
    $Clipped_Len = Get_Soft_Clipping_Len ($Alignment_Array[5]);
    $Aligned_Len = $Read_Len - $Clipped_Len;
    
    if($MapQ>=$MapQ_Cutoff && $Score_Diff>=$Score_Diff_Cutoff && $Best_Score>=$Score_Alignment_Ratio*$Aligned_Len){
        $HQ_Flag = "T";
    }
    return $HQ_Flag;
}

1;
