#!/bin/bash

###1. Simulation of Illumina paired end short reads (by using art_illumina), with 100bp in length, and on average 500bp insert size, 30X coverage
/PathTo/art_illumina -ss HS20 -i ./Target_Seq_Extraction.fa -p -l 100 -f 30 -m 500 -s 10 -o ./Target_Seq_Extraction_100bp_30X
