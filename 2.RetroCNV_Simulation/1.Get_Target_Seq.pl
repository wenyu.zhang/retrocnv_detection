#!/usr/bin/perl -w
use strict;

####This script is used to get the retroCNV parental gene sequence and insertion site, with 1kb flanking regions on both sides
#1) RetroCNV parental gene ENSMUSG00000000001 3(-):108107280-108146146; Only one transcript for this gene (ENSMUST00000000001, with 9 annotated exons)
#2) Assignment of insertion site in the reference genome: 8(+):114193599

##Input file, outputfile, and running parameter#############################
my $Gene_Pos = "3:-:108107280:108146146";
my $Insertion_Site = "8:+:114193599";
my $Flanking_Size = 1000; ##1000bp on each flanking side
my $Reference_Genome = "../1.Create_Exon_Junction_Lib/Mus_musculus.GRCm38.dna.primary_assembly.fa";
my $CDS_File = "ENSMUST00000000001_cDNA.fa";
open(OUT, ">Target_Seq_Extraction.fa");
#############################################################################

######Step 1: store the seq for each chr into hash #######################
my $Chr_ID = "";
my $Chr_Seq = "";
my %Chr_Seq = ();
open(IN_1, "$Reference_Genome");
my @Ref_Seq = <IN_1>; chomp @Ref_Seq;
close IN_1;
for(my $i=0; $i<@Ref_Seq; $i++){
    if($Ref_Seq[$i] =~ /^>/){ ##Header line
        if($i==0){ ##First line of the file
            $Chr_ID = substr($Ref_Seq[$i],1,index($Ref_Seq[$i]," ")-1);
        }
        else{
            $Chr_Seq{$Chr_ID} = $Chr_Seq;
            $Chr_ID = substr($Ref_Seq[$i],1,index($Ref_Seq[$i]," ")-1);
            $Chr_Seq = "";
        }
    }
    else{
        $Chr_Seq = "$Chr_Seq$Ref_Seq[$i]";
        if($i==scalar(@Ref_Seq)-1){ ##last line of the file
            $Chr_Seq{$Chr_ID} = $Chr_Seq;
            $Chr_Seq = "";
        }
    }
}
@Ref_Seq = (); #Release the space
##########################################################################

######Step 2: Get the CDS seq into variable #############################
my $CDS_Seq = "";
open(IN_2, "$CDS_File");
my @CDS_Seq = <IN_2>; chomp @CDS_Seq;
close IN_2;
$CDS_Seq = $CDS_Seq[1]; #First line is header
#########################################################################

######Step 3: Get the retroCNV parental gene sequence and 1000 bp flanking regions on both sides
my @Gene_Pos_Array = split(/\:/,$Gene_Pos);
my $Gene_Chr = $Gene_Pos_Array[0];
my $Start_Pos = $Gene_Pos_Array[2]-$Flanking_Size;
my $End_Pos = $Gene_Pos_Array[3]+$Flanking_Size;
my $Target_Gene_Seq = substr($Chr_Seq{$Gene_Chr},$Start_Pos,$End_Pos-$Start_Pos+1);
print OUT ">Chr$Gene_Chr\:$Start_Pos\-$End_Pos\n";
print OUT "$Target_Gene_Seq\n";

#####Step 4: Get the flanking regions of the insertion site, as well the CDS sequencing it self
my @Insertion_Site_Array = split(/\:/,$Insertion_Site);
my $Insertion_Chr = $Insertion_Site_Array[0];
my $Insertion_Pos = $Insertion_Site_Array[2];
my $Left_Flanking_Seq = substr($Chr_Seq{$Insertion_Chr},$Insertion_Pos-$Flanking_Size,$Flanking_Size);
my $Right_Flanking_Seq = substr($Chr_Seq{$Insertion_Chr},$Insertion_Pos,$Flanking_Size);
print OUT ">Chr$Insertion_Chr\:$Insertion_Pos\_CDS\n";
print OUT "$Left_Flanking_Seq$CDS_Seq$Right_Flanking_Seq\n";
close OUT;
###############################################################################################
