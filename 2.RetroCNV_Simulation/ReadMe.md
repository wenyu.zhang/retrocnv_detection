## This document shows a simple example on the simulation of gene retroposition event in the genome

Required program:
* art_illumina

### Step 1: selection of retroCNV parental gene and insertion site
* RetroCNV parental gene ENSMUSG00000000001 3(-):108107280-108146146; Only one transcript for this gene (ENSMUST00000000001, with 9 annotated exons)
* Assignment of insertion site in the reference genome: 8(+):114193599


### Step 2: Extract the sequence (with 1kb flanking regions on both sides) of the retroCNV parental gene and insertion site
> perl ./1.Get_Target_Seq.pl

### Step 3: Simulation of Illumina short read dataset based on the above extracted dataset 
> bash ./2.Short_Read_Simulation.sh
